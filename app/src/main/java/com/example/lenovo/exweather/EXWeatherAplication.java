package com.example.lenovo.exweather;

import android.app.Application;

/**
 * Created by Lenovo on 16.03.2018.
 */

public class EXWeatherAplication extends Application {
    private static EXWeatherAplication instance;

    public static EXWeatherAplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
