package com.example.lenovo.exweather.entities;

/**
 * Created by Lenovo on 15.03.2018.
 */

public class Coord {

    private double lon;
    private double lat;

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }


}
