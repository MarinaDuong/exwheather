package com.example.lenovo.exweather.entities;

/**
 * Created by Lenovo on 15.03.2018.
 */

public class Sys {
    private int type;
    private long id;
    private String message;
    private String country;
    private long sunrise;
    private long sunset;

    public int getType() {
        return type;
    }

    public long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public String getCountry() {
        return country;
    }

    public long getSunrise() {
        return sunrise;
    }

    public long getSunset() {
        return sunset;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setSunrise(long sunrise) {
        this.sunrise = sunrise;
    }

    public void setSunset(long sunset) {
        this.sunset = sunset;
    }

}
