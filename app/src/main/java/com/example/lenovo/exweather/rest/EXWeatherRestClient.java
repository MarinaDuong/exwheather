package com.example.lenovo.exweather.rest;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.lenovo.exweather.EXWeatherAplication;
import com.example.lenovo.exweather.entities.CityWeather;
import com.example.lenovo.exweather.utils.Helpers;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lenovo on 16.03.2018.
 */

public class EXWeatherRestClient {

    private String CITY = "London,uk";
    private String APP_ID = "b6907d289e10d714a6e88b30761fae22";
    private String TAG = "EXWeatherRestClient";

    public void getCityWeatherData(@NonNull final ResultListener<CityWeather> resultListener) {
        if (!Helpers.hasConnection(EXWeatherAplication.getInstance())) {
            resultListener.onInternetConnectionClosed();
            return;
        }
        WeatherRestClient.getOpenweathermapApiInstance().getCityWeatherData(CITY, APP_ID).enqueue(new Callback<CityWeather>() {
            @Override
            public void onResponse(Call<CityWeather> call, Response<CityWeather> response) {
                if (response.isSuccessful()) {
                    resultListener.onSuccess(response.body());
                } else {
                    resultListener.onError(response.code());
                }
            }

            /*
            Invoked when a network exception occurred talking to the server or when an unexpected exception occurred creating the request or processing the response.
             */
            @Override
            public void onFailure(Call<CityWeather> call, Throwable t) {

                Log.i(TAG, "Fail!");
            }
        });
    }


    public static abstract class ResultListener<T> {
        public abstract void onSuccess(T result);

        public abstract void onConnectionFailure();

        public abstract void onError(int code);

        public abstract void onInternetConnectionClosed();
    }

}
