package com.example.lenovo.exweather.rest;

import com.example.lenovo.exweather.entities.CityWeather;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Lenovo on 15.03.2018.
 */

public interface OpenweathermapApi {
    @GET("/data/2.5/weather")
    Call<CityWeather> getCityWeatherData(@Query("q") String city, @Query("appid") String appid);

}
