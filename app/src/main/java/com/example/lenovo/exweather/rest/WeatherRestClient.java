package com.example.lenovo.exweather.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lenovo on 15.03.2018.
 */

public abstract class WeatherRestClient {
    private static OpenweathermapApi openweathermapApiInstance;

    public static OpenweathermapApi getOpenweathermapApiInstance() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://samples.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        openweathermapApiInstance = retrofit.create(OpenweathermapApi.class);

        return openweathermapApiInstance;
    }
}