package com.example.lenovo.exweather.ui;

import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Toast;

import com.example.lenovo.exweather.R;
import com.example.lenovo.exweather.entities.CityWeather;
import com.example.lenovo.exweather.rest.EXWeatherRestClient;
import com.example.lenovo.exweather.utils.DialogError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraMoveListener, GoogleMap.OnMapClickListener {

    private GoogleMap mMap;
    private LatLng city;
    private String TAG = "MapsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install - А мне нужно самой показывать диалог Установите гугл плей сервисы?????
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //googleMap.getUiSettings().setZoomControlsEnabled(true);
        showWeatherOnMap();
    }

    private void showWeatherOnMap() {

        EXWeatherRestClient exWeatherRestClient = new EXWeatherRestClient();
        exWeatherRestClient.getCityWeatherData(new EXWeatherRestClient.ResultListener<CityWeather>() {
            @Override
            public void onSuccess(CityWeather result) {
                city = new LatLng(result.getCoord().getLat(), result.getCoord().getLon());
                String iconId = result.getWeather().get(0).getIcon();
                final String name = result.getName();
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(MapsActivity.this));
                //TODO StringBuilder
                String imageUri = "http://openweathermap.org/img/w/" + iconId + ".png";
                imageLoader.loadImage(imageUri, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                        mMap.addMarker(new MarkerOptions().position(city).title("Marker in " + name).icon(BitmapDescriptorFactory.fromBitmap(loadedImage)));
//                        mMap.moveCamera(CameraUpdateFactory.newLatLng(city));
//                        mMap.getUiSettings().setZoomControlsEnabled(true);
                        //mMap.moveCamera(CameraUpdateFactory.zoomTo(4));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(city, 4));
                        mMap.setOnCameraMoveListener(MapsActivity.this);
                        mMap.setOnMapClickListener(MapsActivity.this);
                    }
                });


            }

            @Override
            public void onConnectionFailure() {
            }

            @Override
            public void onError(int code) {
                String errorMessage;
                switch (code) {
                    case 401:
                        errorMessage = getString(R.string.error_401);
                        break;
                    case 403:
                        errorMessage = getString(R.string.error_403);
                        break;
                    case 404:
                        errorMessage = getString(R.string.error_404);
                        break;
                    default:
                        errorMessage = getString(R.string.error_default);
                        break;
                }
                Bundle args = new Bundle();
                args.putString(getString(R.string.key_error_message), errorMessage);
                FragmentManager fragmentManager = getSupportFragmentManager();
                DialogError dialogError = new DialogError();
                dialogError.setArguments(args);
                dialogError.show(fragmentManager, "DialogError");
            }

            @Override
            public void onInternetConnectionClosed() {
            }
        });
    }

    @Override
    public void onCameraMove() {
        CameraPosition cameraPosition = mMap.getCameraPosition();
        if (cameraPosition.zoom > 10) {
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        } else {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        CameraPosition cameraPosition = mMap.getCameraPosition();
        Toast.makeText(getApplicationContext(), "!!" + cameraPosition.zoom + "!!", Toast.LENGTH_LONG);
    }
}
