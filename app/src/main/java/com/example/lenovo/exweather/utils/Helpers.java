package com.example.lenovo.exweather.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;

/**
 * Created by Lenovo on 16.03.2018.
 */

public class Helpers {

    public static boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        //get all networks information
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = cm.getAllNetworks();
            int i;

            //checking internet connectivity
            for (i = 0; i < networks.length; ++i) {
                if (cm.getNetworkInfo(networks[i]).getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
            return false;
        } else {
            NetworkInfo wifiInfo = cm.getActiveNetworkInfo();
            return wifiInfo != null;
        }
    }

    public static String getMeaningCode(int code) {
        //https://gist.github.com/karlstolley/99b733313a9a13782843
        String meaningCode = "";
        switch (Helpers.getFirstDigit(code)) {
            case 2:
                meaningCode = "Thunderstorm/Гроза";
                break;
            case 3:
                meaningCode = "Drizzle/Изморозь";
                break;
            case 5:
                meaningCode = "Rain/Дождь";
                break;
            case 6:
                meaningCode = "Snow/Снег";
                break;
            case 7:
                meaningCode = "Thunderstorm/Гроза";
                break;
            case 9:
                if (code > 950 && code < 956) {
                    meaningCode = "Бриз";
                } else {
                    meaningCode = "Осторожно, экстремальная погода!";
                }
                break;
        }
        switch (code) {
            case 701:
            case 721:
            case 741:
                meaningCode = "mist/туман";
                break;
            case 711:
                meaningCode = "smoke/дым";
                break;
            case 731:
            case 751:
            case 761:
                meaningCode = "dust whirls/пылевые завихрения";
                break;
            case 762:
                meaningCode = "volcanic ash/вулканический пепел";
                break;
            case 771:
                meaningCode = "squalls/шквал";
                break;
            case 781:
                meaningCode = "tornado/торнадо";
                break;
            case 800:
                meaningCode = "clear sky/чистое небо";
                break;
            case 801:
            case 802:
            case 803:
            case 804:
                meaningCode = "some clouds/облачно";
                break;
        }
        return meaningCode;
    }

    public static int getFirstDigit(int number) {
        return Integer.parseInt(Integer.toString(number).substring(0, 1));
    }

}
